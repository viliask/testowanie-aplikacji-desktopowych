﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.WindowsAPI;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest3
    {
        [TestMethod]
        public void TestMethod1()
        {

            System.Threading.Thread.Sleep(5000);

            Application application = Application.Launch("C:\\Program Files\\7-Zip\\7zFM.exe");

            Window window1 = application.GetWindow("C:\\Users\\Nalesniki\\Desktop\\testdir\\", InitializeOption.NoCache);


            window1.Keyboard.HoldKey(KeyboardInput.SpecialKeys.ALT);

            window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.F3);

            window1.Keyboard.LeaveKey(KeyboardInput.SpecialKeys.ALT);

            System.Threading.Thread.Sleep(1000);

            window1.Keyboard.HoldKey(KeyboardInput.SpecialKeys.ALT);

            window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.F4);

            window1.Keyboard.LeaveKey(KeyboardInput.SpecialKeys.ALT);

            System.Threading.Thread.Sleep(1000);


            window1.Keyboard.HoldKey(KeyboardInput.SpecialKeys.ALT);

            window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.F5);

            window1.Keyboard.LeaveKey(KeyboardInput.SpecialKeys.ALT);

            System.Threading.Thread.Sleep(1000);

            window1.Keyboard.HoldKey(KeyboardInput.SpecialKeys.ALT);

            window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.F6);

            window1.Keyboard.LeaveKey(KeyboardInput.SpecialKeys.ALT);

            System.Threading.Thread.Sleep(1000);

            window1.Keyboard.HoldKey(KeyboardInput.SpecialKeys.ALT);

            window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.F7);

            window1.Keyboard.LeaveKey(KeyboardInput.SpecialKeys.ALT);

            System.Threading.Thread.Sleep(1000);

            window1.Keyboard.HoldKey(KeyboardInput.SpecialKeys.ALT);

            window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.F3);

            window1.Keyboard.LeaveKey(KeyboardInput.SpecialKeys.ALT);
            int a = 1;

            Assert.AreEqual(1, a);
        }
    }
}
