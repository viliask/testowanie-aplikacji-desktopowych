﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Automation;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.TableItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.WindowsAPI;
//using WindowsInput;
//using WindowsInput.Native;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void TestMethod1()
        {

            System.Threading.Thread.Sleep(5000);

            Application application = Application.Launch("C:\\Program Files\\7-Zip\\7zFM.exe");

            Window window1 = application.GetWindow("C:\\Users\\Nalesniki\\Desktop\\testdir\\", InitializeOption.NoCache);

            window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.F7);
            window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.BACKSPACE);

            window1.Keyboard.Enter("testy");

            List<Window> modalWindows1 = window1.ModalWindows(); //list of all the modal windows belong to the window.
            Window childWindow1 = window1.ModalWindow("Utwórz folder");

            System.Threading.Thread.Sleep(1000);

            Button button11 = childWindow1.Get<Button>("OK");
            button11.Click();

            int a;

            for (a = 0; a < 5; a++)
            {


                window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.UP);

                System.Threading.Thread.Sleep(1000);

                Button button = window1.Get<Button>("Przenieś");
                button.Click();

                System.Threading.Thread.Sleep(1000);

                window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RIGHT);

                window1.Keyboard.Enter("testy/");

                System.Threading.Thread.Sleep(1000);

                //var window = window1.ModalWindow("Dodaj do archiwum");

                List<Window> modalWindows = window1.ModalWindows(); //list of all the modal windows belong to the window.
                Window childWindow = window1.ModalWindow("Przenieś");

                Button button3 = childWindow.Get<Button>("OK");
                button3.Click();

                System.Threading.Thread.Sleep(1000);

                
            }

            Assert.AreEqual(4, a);
        }
    }
}
