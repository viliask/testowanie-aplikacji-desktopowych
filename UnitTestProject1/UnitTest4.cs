﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.White.Factory;
using TestStack.White.UIItems.WindowItems;
using static System.Net.Mime.MediaTypeNames;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.WindowsAPI;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest4
    {
        [TestMethod]
        public void TestMethod1()
        {

            System.Threading.Thread.Sleep(5000);

            TestStack.White.Application application = TestStack.White.Application.Launch("C:\\Program Files\\7-Zip\\7zFM.exe");

            Window window1 = application.GetWindow("C:\\Users\\Nalesniki\\Desktop\\testdir\\", InitializeOption.NoCache);

            int a;

            for (a = 0; a < 2; a++)
            {

                window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DOWN);
                window1.Keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DOWN);

                Button buttonU = window1.Get<Button>("Usuń");
                buttonU.Click();

                System.Threading.Thread.Sleep(1000);

                Window window = application.GetWindow("Usuń plik", InitializeOption.NoCache);

                System.Threading.Thread.Sleep(1000);

                Button button2 = window.Get<Button>("Tak");
                button2.Click();

            }


            Assert.AreEqual(1, a);

        }
    }
}
