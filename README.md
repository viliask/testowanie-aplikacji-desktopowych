# Co zawiera projekt #

Projekt "Testowanie aplikacji desktopowych" zawiera 4 testy automatyczne oraz 10 scenariuszy testowych.
Testowanym aplikacją jest 7 Zip File Manager w wersji: 17.1 beta.

### Szczegóły ###

* Środowisko to VS C#
* Wykorzystany framework: White Framework
* Testowanie polega na wyzwalaniu funkcji testowanego programu za pomocą wirtualnego wciskania przycików klawiatury
* Scenariusze testowe są również testami manualnymi i opisują działanie aplikacji wg tabeli kroków
* Tabela kroków posiada pola: Akcja użytkownika,Wartość oczekiwana oraz Rezultat